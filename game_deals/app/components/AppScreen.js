import React from "react";
import { StyleSheet, SafeAreaView, View } from "react-native";
import AppText from "./AppText";

function AppScreen({ children, style , title}) {
	return (
		<SafeAreaView style={[styles.screen, style]}>
		<AppText style={{color:'black',marginTop:20,marginLeft:30,marginBottom:5,fontSize:20,fontWeight:'200'}}>{title}</AppText>
			<View style={[styles.view, style]}>{children}</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
	view: {
		flex: 1,
	},
});

export default AppScreen;
