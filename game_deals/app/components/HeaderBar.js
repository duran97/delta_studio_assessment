
// External imports.
import React from "react";
import { StyleSheet , View , Image } from "react-native";

// Header to be used on stack.
const HeaderTitle = () => {
	return (
		<View style={styles.container}>
			<Image style={{width:120,height:40}} source={require('../../assets/header_icon_image.png')}></Image>
		</View>
	);
};

export default HeaderTitle;

// Styles.
const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		flex: 1,
		flexDirection: "row",
		height: "100%",
	}
});
