// External imports.
import React from "react";
import { StyleSheet , TouchableOpacity } from "react-native";

// Internal components.
import AppText from './AppText';

const ViewMoreButton = (props) => {

  const { onPressItem , item , borderColor , textColor } = props;

	return (
        <TouchableOpacity style={[styles.viewMoreButton,{borderColor:borderColor}]} onPress={() => onPressItem(item)}>
            <AppText style={[styles.viewMoreButtonText,{color:textColor}]}>View More</AppText>
        </TouchableOpacity>
	);
};

export default ViewMoreButton;

const styles = StyleSheet.create({
    viewMoreButton: {
      marginLeft:15,
      marginTop:50,
      width:150,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'white',
      borderRadius:10,
      borderWidth: 1,
      borderColor: 'black'
    },
    viewMoreButtonText:{
      color:'black',
      textAlign:'center'
    }
  });
