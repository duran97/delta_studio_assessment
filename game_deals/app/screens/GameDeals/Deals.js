// External imports
import React,{useState,useEffect} from "react";
import {  StyleSheet, RefreshControl, View, FlatList, Dimensions, TouchableOpacity , Image } from "react-native";
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';
import { SearchBar , CheckBox} from 'react-native-elements';

// Internal components
import AppScreen from '../../components/AppScreen';
import AppText from '../../components/AppText';
import Utils from '../../utils/index'; 
import ViewMoreButton from '../../components/ViewMoreButton';

const Deals = ({route, navigation}) => {

  const [state, setState] = useState({
    gameDealsData :[],
    isLoading:false,
    searchText:'',
    refreshing:false,
    isOnSaleChecked:true,
    isHideFilter:false
  })

  useEffect(() => {

    let storeId = '';
    if (route.params?.storeId) {
      storeId = route.params?.storeId;
    }

    // Check network connectivity before executing api call. 
    NetInfo.fetch().then(state => {

    if(state.isConnected) {
      setState({...state,isLoading:true})
      RetrieveGameDeals(state.isOnSaleChecked, storeId);
    }
    else {
      alert('No Connectivity')
    }

    })
  
    return () => {
    };
  }, [route.params?.storeId]);

  ListEmpty = () => {
		return (
		  //View to show when list is empty
      <View style={[{marginTop:5}]}>
        <AppText style={{ textAlign: 'center' }}>{!state.isLoading && 'No Deals Available'}</AppText>
      </View>
		);
	};

  const onExpandFilter = () => {

    // Show the filter options view. 
    setState(prevState => ({
      isHideFilter : !prevState.isHideFilter,
      gameDealsData:prevState.gameDealsData,
      isOnSaleChecked:prevState.isOnSaleChecked
    }))
  }

  const onCheckedPress = () => {

    setState({...state,isLoading:true})

    // Change previous checked value state and parse to Deals api call
    let newChecked = !state.isOnSaleChecked;
    RetrieveGameDeals(newChecked);
  }
  
  const onPressItem = (item) => {

    RetrieveStores(item);
  }

  async function RetrieveGameDeals(newChecked, storeId){

    // Call function that executes deals api call and update state.
    try {
        await Utils.GetAllDeals(newChecked, storeId).then(async function (allDeals){

          setState({...state,gameDealsData:allDeals,isLoading:false,isOnSaleChecked:newChecked})
        })
    } catch (error) {
      Alert.alert(error)
    }
  }

  async function RetrieveStores(item)
  {
    // Call function that executes deals api call and update state.
    try {
      await Utils.GetStores().then(async function (stores){

        navigation.navigate('DetailDeals',{ storeId: item.storeID, dealId: item.dealID , allStores: stores })
      })
    } catch (error) {
        alert(error)
    }
  }

  const pullToRrefresh = async () => {

    // Refreshes the list view by recalling the api method.
		setState({...state,refreshing:true});
		await RetrieveGameDeals();
  }
    
  const searchFilterFunction = () => {

    // Check if searched text is not blank
    if (state.searchText) {
      const newData = state.gameDealsData.filter(
        function (item) {
          const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
          const textData = state.searchText.toUpperCase();
          return itemData.includes(textData)
      });
      setState({...state,gameDealsData:newData});
    }
  };

  const updateSearch = (search) => {

    // Update search text state variable. 
    setState({...state,searchText:search });
  };

  return (
    <AppScreen title={'Deals'} style={[styles.container,{backgroundColor:'white'}]}>     
    <Spinner
				visible={state.isLoading}
				textContent={'Loading...'}
			/>  

      <SearchBar
        placeholder="Search deals by name"
        onChangeText={updateSearch}
        round={true}
        onSubmitEditing={()=> searchFilterFunction(state.searchText) }
        containerStyle={{backgroundColor:'white',borderBottomColor: 'transparent',borderTopColor: 'transparent',padding:40}}
        inputContainerStyle={{backgroundColor:'white', borderRadius: 8, borderWidth: 1, borderBottomWidth:1}}
        placeholderTextColor={'gray'}
        inputStyle={{fontSize:12}}
        value={state.searchText}
      />
      <View style={{flexDirection:'row',justifyContent:'flex-end', backgroundColor:'#F0F0F0',paddingRight:20, paddingTop:10, height:40, marginTop:10,marginLeft:30,marginRight:30}}>
      <TouchableOpacity onPress={() => onExpandFilter()} style={{width:Dimensions.get("window").width - 60}}>
            <AppText numberOfLines={1} style={{ color:'black' , alignSelf:'flex-start', fontSize:15,marginLeft:60,fontWeight:'normal' }}>Filters</AppText>
       </TouchableOpacity>
       <Image source={state.isHideFilter ? require('../../../assets/up_arrow.png') :  require('../../../assets/down_arrow.png')}/>   
      </View>

      {
        state.isHideFilter &&
        <View>
          <CheckBox
            center
            iconRight
            containerStyle={{backgroundColor:'white',borderColor:'white', alignSelf:'flex-start'}}
            textStyle={{fontWeight:'normal',marginRight:100}}
            title='On Sale'
            onPress={() => {onCheckedPress()}}
            checked={state.isOnSaleChecked}
          />  
          <View style={{height:1, backgroundColor:'#F0F0F0',width:Dimensions.get("window").width - 30, marginLeft:15}}/>
        </View>
      }
     
      <FlatList
        data={state.gameDealsData}
        ListEmptyComponent={ListEmpty}
        refreshControl={
                <RefreshControl
                  refreshing={state.refreshing}
                  onRefresh={pullToRrefresh.bind(this)}
                />}
        renderItem={({ item, index }) => (
            <View style={styles.listItem}>
                <AppText numberOfLines={1} style={{ color:'black',width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >{item.title}</AppText>
                {
                  // Check if a sale has been given to the item, if it has, show the new price.
               item.isOnSale ?
                <View style={{flexDirection:'row', marginTop:5}}>
                  <AppText numberOfLines={1} style={{ color:'black', fontSize:12,fontWeight:'normal',marginLeft:15,textDecorationLine: 'line-through'}}>{'$' + item.normalPrice}</AppText>
                  <AppText numberOfLines={1} style={{ color:'green', fontSize:12,fontWeight:'normal',marginLeft:15}}>{'$' + item.salePrice}</AppText>
                </View>
                :
                <View style={{flexDirection:'row', marginTop:10}}>
                    <AppText numberOfLines={1} style={{ color:'black', fontSize:12,fontWeight:'normal',marginLeft:15}}>{'$' + item.normalPrice}</AppText>
                </View>
                }
                <ViewMoreButton onPressItem={onPressItem} item={item} borderColor={'black'} textColor={'black'}/>
            </View>
            )}
            keyExtractor={(item) => item.dealID}
          />	
      
    </AppScreen>
  );
};

const styles = StyleSheet.create({
  container: {flexDirection:'column'},
  listItem: {
    width: Dimensions.get("window").width - 20,
    marginLeft:10,
    backgroundColor: "#F0F0F0",
    marginTop:10,
    height:200,
		borderRadius: 10
  }
});

export default Deals;
