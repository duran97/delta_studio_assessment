// External imports.
import React,{useState,useEffect} from "react";
import {  StyleSheet, View, FlatList, Dimensions, Alert , Image, TouchableOpacity} from "react-native";
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';

// Internal components.
import AppScreen from '../../components/AppScreen';
import AppText from '../../components/AppText';
import Utils from '../../utils/index'; 

const DealsDetails = ({ route, navigation }) => {

  const [state, setState] = useState({
    dealDetails :{},
    cheaperStores:[],
    stores:[],
    isLoading:false,
    searchText:'',
    refreshing:false,
    storeName:'',
    storeId:''
    
  })

  const { storeId, dealId ,allStores } = route.params;

  useEffect(() => {

      NetInfo.fetch().then(state => {

      if(state.isConnected === true) {
        setState({...state,isLoading:true,dealDetails:{}})
        RetrieveDetailDealInformation();
      }
      else {
        alert('No Connectivity')
      }
    })
  
    return () => {
    };
  }, []);

  ListEmpty = () => {
		return (
		  //View to show when list is empty
      <View style={[{marginTop:5}]}>
        <AppText style={{ textAlign: 'center' }}>{!state.isLoading && 'No Deals Available'}</AppText>
      </View>
		);
	};

  async function RetrieveDetailDealInformation(){

    // Call function that executes deal details api call and update state.
    try {
        await Utils.GetDealDetails(dealId).then(async function (details){

          let detailsGame = details.gameInfo;
          let otherStores = details.cheaperStores;
        
          setState({...state,storeName:GetStoreName(storeId),cheaperStores:otherStores,dealDetails:detailsGame, stores:allStores,isLoading:false})
        })
    } catch (error) {
      Alert.alert(error)
    }
  }

  const onPressItem = async (item) => {

    RetrieveDetailWithValues(item.dealID, allStores, item.storeID)
  }

  async function RetrieveDetailWithValues(dealId, allStores,storeId){

    // Call function that executes deal details api call and update state.
    try {
        await Utils.GetDealDetails(dealId).then(async function (details){

          let detailsGame = details.gameInfo;
          let otherStores = details.cheaperStores;

          setState({...state,storeName:GetStoreName(storeId),cheaperStores:otherStores,stores:allStores,dealDetails:detailsGame,isLoading:false})
        })
    } catch (error) {
      Alert.alert(error)
    }
  }
  const GetStoreName = (storeId) => {
    let store = allStores.filter((item) => item.storeID === storeId)[0];
    return store.storeName;
  }

  return (
    <AppScreen title={state.dealDetails.name} style={[styles.container,{backgroundColor:'white'}]}>     
    <Spinner
				visible={state.isLoading}
				textContent={'Loading...'}
			/>  
      <View style={{flexDirection:'row', marginTop:5,marginLeft:15}}>
        <AppText numberOfLines={1} style={{ color:'black', fontSize:12,fontWeight:'bold',marginLeft:15,textDecorationLine: 'line-through'}}>{'$' + state.dealDetails.retailPrice}</AppText>
        <AppText numberOfLines={1} style={{ color:'green', fontSize:12,fontWeight:'bold',marginLeft:15}}>{'$' + state.dealDetails.salePrice}</AppText>
      </View>
      <AppText numberOfLines={1} style={{ color:'gray', fontSize:12,fontWeight:'normal',marginLeft:32}}>{'You save $' + (Utils.GetSavings(state.dealDetails.retailPrice,state.dealDetails.salePrice))}</AppText>
      <View style={{alignContent:'center',flexDirection:'row', backgroundColor:'#F0F0F0',width:Dimensions.get("window").width - 60, height:80, marginTop:10, marginLeft:30}}>
        <AppText numberOfLines={1} style={{ color:'black' , width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >Available at {state.storeName}</AppText>
      </View>
      <Image
        style={{width:Dimensions.get("window").width - 60, resizeMode:'contain', marginLeft:30, marginTop:10, height:150}}
        source={{uri: state.dealDetails.thumb}}
      />
      <View style={{alignContent:'center',flexDirection:'row', backgroundColor:'#F0F0F0',width:Dimensions.get("window").width - 60, height:80, marginTop:10, marginLeft:30}}>
        <AppText numberOfLines={1} style={{ color:'black' , width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >Other deals for this game</AppText>
      </View>
      <FlatList
        data={state.cheaperStores}
        ListEmptyComponent={ListEmpty}
        renderItem={({ item, index }) => (
            <View style={styles.listItem}>
                <AppText numberOfLines={1} style={{ color:'black',width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >{GetStoreName(item.storeID)}</AppText>
                <View style={{flexDirection:'row', marginTop:5,marginLeft:15}}>
                  <AppText numberOfLines={1} style={{ color:'black', fontSize:12,fontWeight:'bold',textDecorationLine: 'line-through'}}>{'$' + item.retailPrice}</AppText>
                  <AppText numberOfLines={1} style={{ color:'green', fontSize:12,marginLeft:15,fontWeight:'bold'}}>{'$' + item.salePrice}</AppText>
                </View>
                <TouchableOpacity style={styles.viewMoreButton} onPress={() => onPressItem(item)}>
                  <AppText style={styles.viewMoreButtonText}>View More</AppText>
                </TouchableOpacity>
            </View>
            )}
        keyExtractor={(item) => item.dealID}
        />	
    </AppScreen>
  );
};

const styles = StyleSheet.create({
  container: {flexDirection:'column'},
  listItem: {
    width: Dimensions.get("window").width - 20,
    marginLeft:10,
    backgroundColor: "#F0F0F0",
    marginTop:10,
    height:200,
		borderRadius: 10
  },
  viewMoreButton: {
    marginLeft:15,
    marginTop:50,
    width:150,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'white',
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black'
  },
  viewMoreButtonText:{
    color:'black',
    textAlign:'center'
  },
  listItem: {
    width: Dimensions.get("window").width - 20,
    marginLeft:10,
    backgroundColor: "#F0F0F0",
    marginTop:10,
    height:200,
    borderRadius: 10,
    marginBottom:10
  },
  viewMoreButton: {
    marginLeft:15,
    marginTop:50,
    width:150,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'white',
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black'
  },
  viewMoreButtonText:{
    color:'black',
    textAlign:'center'
  }
});

export default DealsDetails;
