
//External imports
import React,{useState,useEffect} from "react";
import {  StyleSheet, RefreshControl, View, FlatList, Dimensions, Image } from "react-native";
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';
import { SearchBar } from 'react-native-elements';

// Internal components
import AppScreen from '../../components/AppScreen';
import AppText from '../../components/AppText';
import Utils from '../../utils/index'; 

const Game = ({route, navigation}) => {

  const [state, setState] = useState({
    searchedGame :[],
    isLoading:false,
    searchText:'batman',
    refreshing:false,
    isOnSaleChecked:true,
    isHideFilter:false
  })

  useEffect(() => {

    // Check network connectivity before executing api call. 
    NetInfo.fetch().then(state => {

        if(state.isConnected) {
        setState({...state,isLoading:true})
        RetrieveGame();
        }
        else {
        alert('No Connectivity')
        }

    })
  
    return () => {
    };
  }, []);

  ListEmpty = () => {
		return (

		  //View to show when list is empty
            <View style={[{marginTop:5}]}>
                <AppText style={{ textAlign: 'center' }}>{!state.isLoading && 'No Deals Available'}</AppText>
            </View>
		);
	};
 
  async function RetrieveGame(){

    // Call function that executes deals api call and update state.
    try {
        await Utils.GetGame(state.searchText).then(async function (game){

          setState({...state,searchedGame:game,isLoading:false})
        })
    } catch (error) {
      Alert.alert(error)
    }
  }

  const pullToRrefresh = async () => {

    // Refreshes the list view by recalling the api method.
    setState({...state,refreshing:true});
	await RetrieveGame();
  }
    
  const searchFilterFunction = () => {
    RetrieveGame()
  };

  const updateSearch = (search) => {

    // Update search text state variable. 
    setState({...state,searchText:search });
  };

  return (
    <AppScreen title={'Game'} style={[styles.container,{backgroundColor:'white'}]}>     
    <Spinner
		visible={state.isLoading}
		textContent={'Loading...'}
	/>  

      <SearchBar
        placeholder="Search deals by game name"
        onChangeText={updateSearch}
        round={true}
        onSubmitEditing={()=> searchFilterFunction() }
        containerStyle={{backgroundColor:'white',borderBottomColor: 'transparent',borderTopColor: 'transparent',padding:40}}
        inputContainerStyle={{backgroundColor:'white', borderRadius: 8, borderWidth: 1, borderBottomWidth:1}}
        placeholderTextColor={'gray'}
        inputStyle={{fontSize:12}}
        value={state.searchText}
      />
     
      <FlatList
        data={state.searchedGame}
        ListEmptyComponent={ListEmpty}
        refreshControl={
                <RefreshControl
                  refreshing={state.refreshing}
                  onRefresh={pullToRrefresh.bind(this)}
                />}
        renderItem={({ item, index }) => (
            <View style={styles.listItem}>
                <Image
                    style={{width:Dimensions.get("window").width - 60, resizeMode:'contain', marginLeft:30, marginTop:10, height:150}}
                    source={{uri: item.thumb}}
                />
                <AppText numberOfLines={1} style={{ color:'black',width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >{item.external}</AppText>
                <AppText numberOfLines={1} style={{ color:'black',width:Dimensions.get("window").width,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >{'Cheapest available price $' + item.cheapest}</AppText>
            </View>
            )}
            keyExtractor={(item) => item.gameID}
          />	
      
    </AppScreen>
  );
};

const styles = StyleSheet.create({
    container: {flexDirection:'column'},
    listItem: {
      width: Dimensions.get("window").width - 20,
      marginLeft:10,
      flexDirection:'column',
      backgroundColor: "#F0F0F0",
      marginTop:10,
      height:300,
      borderRadius: 10
    },
    storeImagePlaceholder: {
      width: Dimensions.get("window").width - 60,
      marginLeft:20,
      flexDirection:'column',
      backgroundColor: "#F0F0F0",
      marginTop:20,
      height:150,
      borderRadius: 10
    },
    viewMoreButton: {
      marginLeft:15,
      marginTop:50,
      width:150,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'white',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#7C9CBF'
    },
    viewMoreButtonText:{
      color:'#7C9CBF',
      textAlign:'center'
    }
  });

export default Game;
