// External imports
import React,{useState,useEffect} from "react";
import { StyleSheet, RefreshControl, View, FlatList, Dimensions, TouchableOpacity } from "react-native";
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';
import { SearchBar } from 'react-native-elements';

// Internal components.
import AppScreen from '../../components/AppScreen';
import AppText from '../../components/AppText';
import Utils from '../../utils/index'; 
import ViewMoreButton from '../../components/ViewMoreButton';

const Store = (props) => {

  const [state, setState] = useState({
    storeData :[],
    isLoading:false,
    searchText:'',
    refreshing:false,
    gameDealsData:[]
  })

  useEffect(() => {

    // Check network connectivity before executing api call. 
    NetInfo.fetch().then(state => {

      if(state.isConnected) {
        setState({...state,isLoading:true})
        RetrieveStores();
      }
      else {
        alert('No Connectivity')
      }
    })
  
    return () => {
    };
  }, []);

  const GetStoresDealsTotal = (storeId) => {

    // Filter through the deals state value and find the matching store id.
    // return all deals for that store and input into array.
    // Set the array of all deals for the store and return the number items in the array. i.e. number of deals for the store
    let totalDeals = state.gameDealsData.filter(function(item){
      return item.storeID === storeId;
      }).map(function({dealID}){
          return {dealID};
      });

      return totalDeals.length;
  }

  ListEmpty = () => {
		return (
		  //View to show when list is empty
      <View style={[{marginTop:5}]}>
        <AppText style={{ textAlign: 'center' }}>{!state.isLoading && 'No Stores Available'}</AppText>
      </View>
		);
  };
  
  const onPressItem = (item) => {

    props.navigation.navigate('Deals', {
      screen: 'Deals',
      params: { storeId: item.storeID },
    });
  }

  async function RetrieveStores(){

    // Call function that executes deals api call and update state.
    try {
        await Utils.GetStores().then(async function (allStores){

          // Call api to retrieve deals. 
          // A function will be called to map through and find the corresponding deals for each store.
          // See GetStoresDealsTotal. 
          RetrieveGameDeals(allStores);
        })
    } catch (error) {
      Alert.alert(error)
    }
  }

  async function RetrieveGameDeals(allStores){

    // Call function that executes deals api call and update state.
    try {
        await Utils.GetAllDeals(true).then(async function (allDeals){

          setState({...state,gameDealsData:allDeals,storeData:allStores,isLoading:false})
        })
    } catch (error) {
      Alert.alert(error)
    }
  }

  const pullToRrefresh = async () => {

    // Refreshes the list view by recalling the api method.
		setState({...state,refreshing:true});
		await RetrieveStores();
  }
    
  const searchFilterFunction = () => {

    // Check if searched text is not blank
    if (state.searchText) {
      const newData = state.storeData.filter(
        function (item) {
          const itemData = item.storeName ? item.storeName.toUpperCase() : ''.toUpperCase();
          const textData = state.searchText.toUpperCase();
          return itemData.includes(textData)
      });
      setState({...state,storeData:newData,searchText:''});
    }
  };

  const updateSearch = (search) => {

    // Update search text state variable. 
    setState({...state,searchText:search });
  };

  return (
    <AppScreen title={'Stores'} style={[styles.container,{backgroundColor:'white'}]}>     
    <Spinner
				visible={state.isLoading}
				textContent={'Loading...'}
			/>  
      <SearchBar
        placeholder="Search stores by name"
        onChangeText={updateSearch}
        round={true}
        onSubmitEditing={()=> searchFilterFunction(state.searchText) }
        containerStyle={{backgroundColor:'white',borderBottomColor: 'transparent',borderTopColor: 'transparent',padding:40}}
        inputContainerStyle={{backgroundColor:'white', borderRadius: 8, borderWidth: 1, borderBottomWidth:1}}
        placeholderTextColor={'gray'}
        inputStyle={{fontSize:12}}
        value={state.searchText}
      />
      
      <FlatList
        data={state.storeData}
        ListEmptyComponent={ListEmpty}
        refreshControl={
                <RefreshControl
                  refreshing={state.refreshing}
                  onRefresh={pullToRrefresh.bind(this)}
                />}
        renderItem={({ item, index }) => (
            <View style={styles.listItem}>
              <View style={[styles.storeImagePlaceholder,{backgroundColor:'#D8D8D8'}]}>
              </View>
              <AppText numberOfLines={1} style={{ color:'black',width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >{item.storeName}</AppText>
              <AppText numberOfLines={1} style={{ color:'black',width:Dimensions.get("window").width - 150,fontWeight:'bold',marginLeft:15, marginTop: 20 }} >{GetStoresDealsTotal(item.storeID)}</AppText>
              <ViewMoreButton onPressItem={onPressItem} item={item} borderColor={'#7C9CBF'} textColor={'#7C9CBF'} />
            </View>
            )}
            keyExtractor={(item) => item.storeID}
          />	
      
    </AppScreen>
  );
};

const styles = StyleSheet.create({
  container: {flexDirection:'column'},
  listItem: {
    width: Dimensions.get("window").width - 20,
    marginLeft:10,
    flexDirection:'column',
    backgroundColor: "#F0F0F0",
    marginTop:10,
    height:400,
		borderRadius: 10
  },
  storeImagePlaceholder: {
    width: Dimensions.get("window").width - 60,
    marginLeft:20,
    flexDirection:'column',
    backgroundColor: "#F0F0F0",
    marginTop:20,
    height:150,
		borderRadius: 10
  }
});

export default Store;
