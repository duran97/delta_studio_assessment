// External Imports.
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";

// Components.
import Store from '../screens/GameDeals/Store';
import HeaderBar from '../components/HeaderBar';

// Create stack hook.
const Stack = createStackNavigator();

// Create about stack.
const StoreStack = () => {
	return (
		<Stack.Navigator 
			screenOptions={{
				headerTitleAlign: "center",
				headerStyle: {
					backgroundColor:'white'
				},
				headerShown: true
			}}
		>
			<Stack.Screen
				name="Store"
				component={Store}
				options={{
					headerTitle: () => <HeaderBar />,
				}}
			/>
		</Stack.Navigator>
	);
};

export default StoreStack;
