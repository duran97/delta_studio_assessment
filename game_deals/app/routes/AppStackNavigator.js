import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";

// Internal components.
import HomeScreen from './AppTabNavigator';

// Create stack hook.
const Stack = createStackNavigator();

// Create about stack.
const HomeStack = () => {
	return (
		<Stack.Navigator
			screenOptions={{
				headerTitleAlign: "center",
				headerStyle: {
					backgroundColor:'white'
				},
				headerShown: true
			}}
		>
			<Stack.Screen
				name="Home"
				component={HomeScreen}
			/>
		</Stack.Navigator>
	);
};

export default HomeStack;
