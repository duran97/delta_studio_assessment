import * as React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import StoreStack from "./StoreStack";
import DealsStack from "./DealsStackNavigator";
import GamesStack from "./GameStackNavigator";

const Tab = createBottomTabNavigator();

export default function AboutTabNavigator() {
  return (
    <Tab.Navigator
      initialRouteName="Deals"
      screenOptions={({ route }) => ({
        headerShown: true
      })}
      tabBarOptions={{
        activeTintColor: "black",
        inactiveTintColor: "gray",
        labelStyle: {
          fontSize:20,        
          marginBottom: 18
        },
        style: {
          borderWidth: 0.5,
          borderBottomWidth: 1,
          backgroundColor: 'white',
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          borderColor: 'grey',
          position: 'absolute'
        },
      }}
    >
      <Tab.Screen name="Deals" component={DealsStack} initialParams={{ storeId: ' ' }} />
      <Tab.Screen name="Games" component={GamesStack} />
      <Tab.Screen name="Store" component={StoreStack} />
    </Tab.Navigator>
  );
}
