import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";

// Components.
import Deals from '../screens/GameDeals/Deals';
import DetailsDeals from '../screens/GameDeals/DetailDeals';
import HeaderBar from '../components/HeaderBar';

// Create stack hook.
const Stack = createStackNavigator();

// Create about stack.
const DealsStack = () => {
	return (
		<Stack.Navigator 
			screenOptions={{
				headerTitleAlign: "center",
				headerTintColor: 'white',
				backgroundColor:'white',
				headerStyle: {
					backgroundColor:'white',
					borderBottomLeftRadius: 30,
					borderBottomRightRadius: 30,
					shadowColor: '#f44242',
					shadowOffset: { width: 0, height: 10 },
					shadowOpacity: 1,
					height: 60
				},
				headerShown: true
		}}
		>
			<Stack.Screen
				name="Deals"
				component={Deals}
				options={{
					headerTitle: () => <HeaderBar />
				}}
			/>
      		<Stack.Screen
				name="DetailDeals"
				component={DetailsDeals}
				options={{
					headerTitle: () => <HeaderBar />
				}}
			/>
		</Stack.Navigator>
	);
};

export default DealsStack;
