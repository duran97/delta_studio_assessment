// External imports.
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";

// Internal components.
import Game from '../screens/GameDeals/Game';
import HeaderBar from '../components/HeaderBar';

// Create stack hook.
const Stack = createStackNavigator();

// Create about stack.
const StoreStack = () => {
	return (
		<Stack.Navigator 
			screenOptions={{
				headerTitleAlign: "center",
				headerStyle: {
					backgroundColor:'white'
				},
				headerShown: true
			}}
		>
			<Stack.Screen
				name="Game"
                component={Game}
                options={{
					headerTitle: () => <HeaderBar />,
				}}
			/>
		</Stack.Navigator>
	);
};

export default StoreStack;
