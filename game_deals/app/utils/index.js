import { Alert } from 'react-native';

async function GetAllDeals(isOnSale,storeId) {

  let api = "https://www.cheapshark.com/api/1.0/deals?onSale ="+isOnSale;

  // Change api call if there is a specific store to search by. 
  if(storeId !==''){
    api = "https://www.cheapshark.com/api/1.0/deals?onSale ="+isOnSale +"&storeID ="+storeId;
  }

  try {
      let response = await fetch(api);

      let responseJson = await response.json();

      return responseJson;

  } catch (error) {
    Alert.alert(error)
  }
}

async function GetDealDetails(dealId) {
  try {

      let response = await fetch("https://www.cheapshark.com/api/1.0/deals?id=" + dealId);

      let responseJson = await response.json();
      
      return responseJson;

  } catch (error) {
    Alert.alert(error)
  }
}

async function GetStores() {
  try {

    let response = await fetch("https://www.cheapshark.com/api/1.0/stores");

    let responseJson = await response.json();

    return responseJson;

    } catch (error) {
      Alert.alert(error)
    }
}

async function GetGame(searchText) {
  try {

    let response = await fetch("https://www.cheapshark.com/api/1.0/games?title="+searchText);

    let responseJson = await response.json();

    return responseJson;

    } catch (error) {
      Alert.alert(error)
    }
}

function GetSavings(retailPrice, salePrice) {

  let convertRetailPrice = parseInt(Number(retailPrice));

  let convertSalePrice = parseInt(Number(salePrice));

  return (convertRetailPrice - convertSalePrice).toFixed(2);
}


export default {
  GetAllDeals,
  GetDealDetails,
  GetStores,
  GetGame,
  GetSavings
}
